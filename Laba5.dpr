program Laba5;

{$APPTYPE CONSOLE}
///
uses
  SysUtils, Math;

const
  E1 = 0.01;
  E2 = 0.001;
  E3 = 0.0001;

var
  I, K: Integer;
  F1, F2, Mult, X, T: Real;
  Flag1, Flag2: Boolean;

begin
  //Output table's header:
  Write(#201);
  for i := 1 to 10 do  Write(#205);
  Write(#203);
  for i := 1 to 10 do  Write(#205);
  Write(#203);
  for i := 1 to 16 do  Write(#205);
  Write(#203);
  for i := 1 to 16 do  Write(#205);
  Write(#203);
  for i := 1 to 16 do  Write(#205);
  Write(#187);

  Writeln;

  write(#186);
  for i := 1 to 10 do  Write(' ');
  Write(#186);
  for i := 1 to 10 do  Write(' ');
  Write(#186);
  for i := 1 to 3 do  Write(' ');
  Write(' E = 10^(-2) ');
  Write(#186);
  for i := 1 to 3 do  Write(' ');
  Write(' E = 10^(-3) ');
  Write(#186);
  for i := 1 to 3 do  Write(' ');
  Write(' E = 10^(-4) ');
  Write(#186);

  Writeln;

  write(#186);
  for i := 1 to 4 do  Write(' ');
  write('X');
  for i := 6 to 10  do write(' ');
  Write(#186);
  for i := 1 to 4 do  Write(' ');
  Write('f1(X)');
  for i := 8 to 8 do write(' ');
  Write(#186);
  for i := 1 to 16 do  Write(' ');
  Write(#186);
  for i := 1 to 16 do  Write(' ');
  Write(#186);
  for i := 1 to 16 do  Write(' ');
  Write(#186);
  WriteLn;

  Write(#186);
  for i := 1 to 10 do Write(' ');
  Write(#186);
  for i := 1 to 10 do Write(' ');
  Write(#204);
  For i := 1 to 7 do Write(#205);
  Write(#203);
  for i := 9 to 16 do write(#205);
  Write(#206);
  For i := 1 to 7 do Write(#205);
  Write(#203);
  for i := 9 to 16 do write(#205);
  Write(#206);
  For i := 1 to 7 do Write(#205);
  Write(#203);
  for i := 9 to 16 do write(#205);
  Write(#185);
  WriteLn;

  write(#186);
  for i := 1 to 10  do write(' ');
  Write(#186);
  for i := 1 to 10 do  Write(' ');
  Write(#186);
  for i := 1 to 7 do  Write(' ');
  Write(#186);
  for i := 9 to 16 do write(' ');
  Write(#186);
  for i := 1 to 7 do  Write(' ');
  Write(#186);
  for i := 9 to 16 do write(' ');
  Write(#186);
  for i := 1 to 7 do  Write(' ');
  Write(#186);
  for i := 9 to 16 do write(' ');
  Write(#186);
  WriteLn;

  write(#186);
  for i := 1 to 10  do write(' ');
  Write(#186);
  for i := 1 to 10 do  Write(' ');
  Write(#186);
  Write(' f2(X) ');
  Write(#186);
  Write('    N   ');
  Write(#186);
  Write(' f2(X) ');
  Write(#186);
  Write('    N   ');
  Write(#186);
  Write(' f2(X) ');
  Write(#186);
  Write('    N   ');
  Write(#186);
  WriteLn;

  write(#186);
  for i := 1 to 10  do write(' ');
  Write(#186);
  for i := 1 to 10 do  Write(' ');
  Write(#186);
  for i := 1 to 7 do  Write(' ');
  Write(#186);
  for i := 9 to 16 do write(' ');
  Write(#186);
  for i := 1 to 7 do  Write(' ');
  Write(#186);
  for i := 9 to 16 do write(' ');
  Write(#186);
  for i := 1 to 7 do  Write(' ');
  Write(#186);
  for i := 9 to 16 do write(' ');
  Write(#186);
  WriteLn;

  write(#204);
  for i := 1 to 10  do write(#205);
  Write(#206);
  for i := 1 to 10 do  Write(#205);
  Write(#206);
  for i := 1 to 7 do  Write(#205);
  Write(#206);
  for i := 9 to 16 do write(#205);
  Write(#206);
  for i := 1 to 7 do  Write(#205);
  Write(#206);
  for i := 9 to 16 do write(#205);
  Write(#206);
  for i := 1 to 7 do  Write(#205);
  Write(#206);
  for i := 9 to 16 do write(#205);
  Write(#186);
  WriteLn;

  write(#186);
  for i := 1 to 10  do write(' ');
  Write(#186);
  for i := 1 to 10 do  Write(' ');
  Write(#186);
  for i := 1 to 7 do  Write(' ');
  Write(#186);
  for i := 9 to 16 do write(' ');
  Write(#186);
  for i := 1 to 7 do  Write(' ');
  Write(#186);
  for i := 9 to 16 do write(' ');
  Write(#186);
  for i := 1 to 7 do  Write(' ');
  Write(#186);
  for i := 9 to 16 do write(' ');
  Write(#186);
  WriteLn;

  X := - 0.6;
  for I := 1 to 20 do
  begin
    //Output X:
    write(#186);
    if x < 0 then
    begin
      Write('  ', X:0:2, '   ');
    end
    else
    begin
      Write('   ', X:0:2, '   ');
    end;
    Write(#186);

    //Calculation the firts function:
    F1 := Tan((Pi * X) / 2);
    //OutPut the first functon:
    if F1 < 0 then
    begin
      Write('  ', F1:0:3, '  ');
    end
    else
    begin
      Write('   ', F1:0:3, '  ');
    end;

    Write(#186);
    Flag1 := True;
    Flag2 := True;
    K := 0;
    F2 := 0;
    T := 100; //To enter the cicle
    Mult := (4 * X) / Pi;
    //Our calculations will be carried out to the lowest accuracy, so
    while Abs(T) > E3 do
    begin
      T := Mult / (Sqr(2 * K - 1) - X * X);
      K := K + 1;
      F2 := F2 + Mult  / (Sqr( 2 * k - 1) - X * X);
      //Calculation and OutPut with the first Precision:
      if (Abs(T) <= E1) and (Flag1) then
      begin
        Flag1 := False;
        if F2 < 0 then
        begin
           Write(F2:0:3, ' ');
        end
        else
        begin
          Write(' ', F2:0:3, ' ');
        end;
        Write(#186);
        Write('    ', K, '   ');
        write(#186);
      end;
      //Calculation and OutPut with the second Precision:
      if (Abs(T) <= E2) and (Flag2) then
      begin
        Flag2 := False;
        if F2 < 0 then
        begin
           Write(F2:0:3, ' ');
        end
        else
        begin
          Write(' ', F2:0:3, ' ');
        end;
        Write(#186);
        if (K div 10) = 0 then
        begin
          Write('    ', K, '   ');
        end
        else
        begin
          Write('   ', K, '   ');
        end;
        Write(#186);
      end;
    end;
    //Calculation and OutPut with the third Precision:
    if F2 < 0 then
    begin
      Write(F2:0:3, ' ');
    end
    else
    begin
      Write(' ', F2:0:3, ' ');
    end;
    Write(#186);
    if K div 10 = 0 then
    begin
      Write('    ', K, '   ');
    end
    else
    begin
       Write('   ', K, '   ');
    end;
    Write(#186);
    WriteLn;

    X := X + 0.05;
  end;

  //OutPut Bottom of the table:
  Write(#200);
  for i := 1 to 10 do  Write(#205);
  Write(#202);
  for i := 1 to 10 do  Write(#205);
  Write(#202);
  for i := 1 to 7 do  Write(#205);
  write(#202);
  For i := 9 to 16 do Write(#205);
  Write(#202);
  for i := 1 to 7 do  Write(#205);
  write(#202);
  For i := 9 to 16 do Write(#205);
  Write(#202);
  for i := 1 to 7 do  Write(#205);
  write(#202);
  For i := 9 to 16 do Write(#205);
  Write(#188);

  Writeln;
  Readln;
end.
